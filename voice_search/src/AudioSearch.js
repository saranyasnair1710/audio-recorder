import React from 'react';

import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import MicIcon from '@material-ui/icons/Mic';
import MicOffIcon from '@material-ui/icons/MicOff';

import {useStyles} from './Styles';
import useRecorder from "./useRecorder";


export default function AudioSearch() {
    let [audioURL, isRecording, startRecording, stopRecording] = useRecorder();
    const classes = useStyles();

    return (
        <div>
            <Paper component="form" className={classes.root}>
                <IconButton className={classes.iconButton} aria-label="menu">
                    <MenuIcon />
                </IconButton>
                <InputBase
                    className={classes.input}
                    placeholder="Type Here"
                    inputProps={{ 'aria-label': 'Type here' }}
                />
                <IconButton type="submit" className={classes.iconButton} aria-label="search">
                    <SearchIcon />
                </IconButton>
                <Divider className={classes.divider} orientation="vertical" />
                <IconButton color="primary" className={classes.iconButton} aria-label="directions">
                    <MicIcon onClick={startRecording} style={{visibility: isRecording? "hidden":"visible"}}/>
                    <MicOffIcon onClick={stopRecording} style={{visibility: isRecording? "visible":"hidden"}}/> 
                </IconButton>
            </Paper>
        </div>
    );
}
