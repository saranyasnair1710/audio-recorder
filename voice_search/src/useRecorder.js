import { useEffect, useState } from "react";

const useRecorder = () => {
  const [audioURL, setAudioURL] = useState("");
  const [isRecording, setIsRecording] = useState(false);
  const [recorder, setRecorder] = useState(null);
  const [data, setData] = useState(null);
  const [audio_chunks, setAudioChunks] = useState([]);

  useEffect(() => {

    if (recorder === null) {
      if (isRecording) {
        requestRecorder().then(setRecorder, console.error);
      }
      return;
    }

    if (isRecording) {
      recorder.start();
    } 
    // else {
    //   recorder.stop();
    // }

  
    const handleData = (e) => {
      audio_chunks.push(e.data)
      // setAudioURL(URL.createObjectURL(e.data));
      setData(e.data);
    };

    console.log(recorder)
    recorder.addEventListener("dataavailable", handleData);
    // return () => recorder.removeEventListener("dataavailable", handleData);
  }, [recorder, isRecording, data]);

  const startRecording = () => {
    setIsRecording(true);
  };

  const stopRecording = async () => {
    setIsRecording(false);
    recorder.stop();
    const blobData = new Blob(audio_chunks, { type: "audio/webm;codecs=opus" });
    console.log("Before Sleep func", blobData);
    // var blob = new Blob(audio_chunks);
    // const imageBlob = URL.createObjectURL(audio_chunks);
    // console.log("Audio", imageBlob);
    // console.log("Blob",blob);
    // var xhttp = new XMLHttpRequest();
    // xhttp.open("POST", "https://sltzzwn6v8.execute-api.us-west-2.amazonaws.com/dev/text", true);
    // var data = new FormData();
    // console.log("Final", audio_chunks);
    // data.append('audio', new Blob(audio_chunks), 'audio_chunks');
    // xhttp.send(data);
    // xhttp.onreadystatechange = function() {
    //     if (this.readyState == 4 && this.status == 200) {
    //         console.log(this.responseText);     
    //      }
    // };
    const name="audio_search"
    var blob={};
     await sleep(5000).then(r => {
          let audio = new File(audio_chunks,`${name}.wav`, {
              type: "audio/wav;codecs=opus"
          });
          blob = new Blob(audio_chunks, { type: "audio/wav;codecs=opus" });

          console.log("Final Blob", blob)
          setAudioURL(URL.createObjectURL(audio));
        })
      const base64 = await getBase64V2(blob)
      var voiceDataBase64="";
      if(base64 !== "" && base64 !== undefined){
        const startIndex = base64.indexOf("base64,") + 7;
        voiceDataBase64 = base64.substr(startIndex);
      }
      let xhttp = new XMLHttpRequest();
      xhttp.open("POST", "https://sltzzwn6v8.execute-api.us-west-2.amazonaws.com/dev/text", true);
      // var data = new FormData();
      // console.log("Final", audio_chunks);
      // data.append('audio', blob, 'blob');
      var data = JSON.stringify({'audio': voiceDataBase64});
      xhttp.send(data);
  };

  return [audioURL, isRecording, startRecording, stopRecording, data];
};

let promise = new Promise(function(resolve, reject) {
  setTimeout(() => resolve(1), 1000);
});


const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

const dummy = async (audio) =>{
  var base64 = await getBase64V2(audio);
  var voiceDataBase64="";
  if(base64 !== "" && base64 !== undefined){
      const startIndex = base64.indexOf("base64,") + 7;
      voiceDataBase64 = base64.substr(startIndex);
      return voiceDataBase64;
  }
}
  const getBase64V2 = (file) => {

    let reader = new FileReader();

    return new Promise((resolve, reject) => {

      reader.readAsDataURL(file); 
      reader.onerror = () => {
        reader.abort();
        reject("Problem parsing input file.");
      };
      reader.onload = () => {
        resolve(reader.result)
      }

    });

  }

async function requestRecorder() {
  const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
  console.log("Stream", stream);
  return new MediaRecorder(stream);
  
}
export default useRecorder;